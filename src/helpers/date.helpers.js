export const formatDateLastMessage = (date) => {
    if (!date) {
        return ''
    }
    const parseDate = Date.parse(date);
    const formatterDate = new Intl.DateTimeFormat("ru", {
        day: 'numeric',
        month: 'numeric',
        year: 'numeric',
    });

    const fornaterTime = new Intl.DateTimeFormat("ru", {
        hour: 'numeric',
        minute: 'numeric',
    });

    return formatterDate.format(parseDate) + ' ' + fornaterTime.format(parseDate);
};

export const formatDataForDivider = (date) => {
    const parseDate = Date.parse(date);

    let formatterWeekday = new Intl.DateTimeFormat("en-US", {
        weekday: 'long',
    });
    let formatterDayNumber = new Intl.DateTimeFormat("en-US", {
        day: 'numeric',
    });
    let formatterMonth = new Intl.DateTimeFormat("en-US", {
        month: 'long',
    });
    return formatterWeekday.format(parseDate) + ', ' + formatterDayNumber.format(parseDate) + ' ' + formatterMonth.format(parseDate);

};

export const formatDataForMessage = (date) => {
    if (!date) {
        return ''
    }
    const parseDate = Date.parse(date);
    let formatter = new Intl.DateTimeFormat("ru", {
        hour: "numeric",
        minute: "numeric",
    });
    return formatter.format(parseDate);
};

export const parseDay = (date) => {
    if (!date) {
        return ''
    }
    const parseDate = new Date(Date.parse(date));
    return parseDate.getDate();
};