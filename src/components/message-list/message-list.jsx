import React from "react";
import Message from "../message/message";
import OwnMessage from "../own-message/own-message";
import './style.css';
import {formatDataForDivider, formatDataForMessage, parseDay} from "../../helpers/date.helpers";

function MessageList({messages = [], userId, deleteMessage, showModalForEdit}) {

    let currentDay;
    const todayNumber = new Date().getDate();



    const dividerMessage = (createdAt) => {
        const messageDay = parseDay(createdAt);
        const dividerText = (+messageDay === +todayNumber)
            ? 'Today'
            : (messageDay === (todayNumber - 1))
                ? 'Yesterday'
                : formatDataForDivider(createdAt);
        if (messageDay !== currentDay) {
            currentDay = messageDay;
            return <div className="messages-divider">{dividerText}</div>;
        }
        return null;
    };

    return (
        <div className="message-list">
            {messages.map(message => {
                message.formatCreatedAt = formatDataForMessage(message.createdAt);
                message.formatEditedAt = formatDataForMessage(message.editedAt);
                return (message.userId === userId)
                    ? <React.Fragment key={message.id}>
                        {dividerMessage(message.createdAt)}
                        <OwnMessage
                            messageInfo={message}
                            deleteMessage={deleteMessage}
                            handleEditMessageId={showModalForEdit}
                        />
                    </React.Fragment>
                    : <React.Fragment key={message.id}>
                        {dividerMessage(message.createdAt)}
                        <Message
                            messageInfo={message}
                        />
                    </React.Fragment>

            })}

        </div>
    );
}

export default MessageList;