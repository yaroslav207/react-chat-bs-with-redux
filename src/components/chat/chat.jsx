import React, {useEffect} from "react";
import {useDispatch, useSelector} from 'react-redux';

import Header from "../header/header";
import MessageList from "../message-list/message-list";
import MessageInput from "../message-input/message-input";
import Preloader from "../preloader/preloader";
import EditMessageModal from "../edit-message-modal/EditMessageModal";

import {formatDateLastMessage} from "../../helpers/date.helpers";
import {loadMessages, addMessage, editMessage, deleteMessage, setIdEditMessage, closeModal} from "../../store/actions";

import './style.css'

function Chat({url}) {
    const dispatch = useDispatch();

    const myUserId = '5328dba1-1b8f-11e8-9629-c7eca82aa7bd';
    const messages = useSelector(state => state.messages);
    const preloader = useSelector(state => state.preloader);

    const getUser = (id) => {
        const message = messages.find(message => message.userId === id);
        if (!message) {
            return {userId: myUserId}
        }

        const {userId, user, avatar} = message;
        return {userId, user, avatar}
    };
    const getMessage = (id) => {
        return messages.find(message => message.id === id)
    };
    const getUserCount = (messages) => {
        const result = [];
        messages.forEach((message) => {
            if (!result.includes(message.user)) {
                result.push(message.user);
            }
        });

        return result.length
    };
    const getDateLastMessage = (messages) => {
        let result = messages[0].createdAt;
        messages.forEach(message => {
            if (message.createdAt > result) {
                result = message.createdAt;
            }
        });

        return result
    };
    const getLastMyMessage = (messages) => {
        return [...messages].reverse().find(messages => messages.userId === myUserId)
    };

    const handleEditLastMessage = (e) => {
        if (e.code === 'ArrowUp') {
            const message = getLastMyMessage(messages);
            showModalForEdit(message.id);
        }
    };


    const handleDeleteMessage = (id) => {
        dispatch(deleteMessage(id))
    };
    const handleAddMessage = (messageText) => {
        if (!messageText) {
            return
        }
        const user = getUser(myUserId);
        const newMessage = {
            ...user,
            id: Date.parse(new Date().toISOString()) ,
            text: messageText,
            createdAt: new Date().toISOString(),
            editedAt: '',
        };

        dispatch(addMessage(newMessage));
    };
    const showModalForEdit = (id) => {
        dispatch(setIdEditMessage(id))
    };
    const closeModalForEdit = () => {
        dispatch(closeModal())
    };
    const handleEditMessage = (id, text) => {
        if (!id || !text) {
            return
        }
        dispatch(editMessage({id, text}))
    };

    useEffect(() => {
        fetch(url)
            .then(result => result.json())
            .then(json => {
                dispatch(loadMessages(json));
            });
    }, []);

    useEffect(() => {
        if (!preloader) {
            document.body.addEventListener("keydown", handleEditLastMessage);
        }
        return () => {
            document.body.removeEventListener("keydown", handleEditLastMessage);
        }
    }, [messages]);

    return (!preloader)
        ? (
            <div className="chat">
                <Header
                    usersCount={getUserCount(messages)}
                    messageCount={messages.length}
                    lastMessageDate={formatDateLastMessage(getDateLastMessage(messages))}
                />
                <MessageList
                    messages={messages}
                    userId={myUserId}
                    deleteMessage={handleDeleteMessage}
                    showModalForEdit={showModalForEdit}
                />
                <MessageInput
                    addMessage={handleAddMessage}
                />
                <EditMessageModal
                    closeModal={closeModalForEdit}
                    editMessage={handleEditMessage}
                    getMessage={getMessage}
                />
            </div>
        )
        : <Preloader/>
}

export default Chat;