const loadMessages = (messages) => ({
    type: 'LOAD_MESSAGES',
    payload: messages
});

const addMessage = (message) => {
  return {
    type: 'ADD_MESSAGE',
    payload: message
  };
};

const deleteMessage = (id) => ({
  type: 'DELETE_MESSAGE',
  payload: id
});

const editMessage = ({ id, text }) => ({
  type: 'EDIT_MESSAGE',
  payload: { id, text }
});

const setIdEditMessage = (id) => ({
  type: 'SET_ID_EDIT_MESSAGE',
  payload: id
});

const closeModal = () => ({
  type: 'CLOSE_MODAL'
});

export {loadMessages, addMessage, deleteMessage, editMessage, setIdEditMessage, closeModal};
