const initialState = {
    messages: [],
    editModal: false,
    preloader: true,
    editMessageId: ''
};

const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'LOAD_MESSAGES':
            return {
                ...state,
                messages: action.payload,
                preloader: false
            };
        case 'ADD_MESSAGE':
            return {
                ...state,
                messages: [...state.messages, action.payload],
            };
        case 'DELETE_MESSAGE':
            return {
                ...state,
                messages: state.messages.filter(message => message.id !== action.payload),
            };

        case 'SET_ID_EDIT_MESSAGE':
            return {
                ...state,
                editMessageId: action.payload,
                editModal: true
            };

      case 'CLOSE_MODAL':
        return {
          ...state,
          editMessageId: '',
          editModal: false
        };

        case 'EDIT_MESSAGE':
            return {
                ...state,
                messages: state.messages.map(message => {
                    if (message.id === action.payload.id) {
                        message.text = action.payload.text;
                        message.editedAt = new Date().toISOString();
                    }
                    return message
                }),
                editMessageId: '',
                editModal: false
            };

        default:
            return state
    }
};

export { messagesReducer }
