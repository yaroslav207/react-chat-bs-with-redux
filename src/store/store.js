import { createStore } from 'redux';
import   { messagesReducer }   from './root-reducer';

const store = createStore( messagesReducer );

export default store;
