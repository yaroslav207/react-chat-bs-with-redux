import Chat from './src/components/chat/chat'
import { messagesReducer as rootReducer } from './src/store/root-reducer';

export default {
    Chat,
    rootReducer,
}